#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_no_mp_10 import PaymentNoMp10
from TestData.Brana_test_data import HomePageData
import time
import pytest

class Test10(BaseClass):

    def test_standard_payment(self, getData):
        # Standardní platba: karta + mpass@bank, bez pt@bank
        tc10 = PaymentNoMp10(self.driver)
        # logger
        log = self.getLogger()
        tc10.select_test().click()
        tc10.confirm_test().click()
        self.explicit_wait_function_ID('creditcard')
        payment_buttons = tc10.payment_methods()
        payments = ['button-pay-csob', 'button-pay-era']
        try:
            for pb in payment_buttons:
                assert pb.get_attribute("class") in payments
            assert len(payment_buttons) == 2
            log.info("Dostupna pouze platebni tlacitka CSOB a ERA - podle scenare OK")
            not_found = True
        except:
            log.warning("Dostupne spatne platebni metody - NOK")
            not_found = False
        assert not_found
        tc10.fill_card_number().send_keys(getData["card_number"])
        time.sleep(1)
        tc10.click_show_popup_error().click()
        tc10.fill_expiration().send_keys(getData["expiration"])
        tc10.fill_cvc().send_keys(getData["cvc"])
        log.info("Card number: " + getData["card_number"] + " Expiration: " + getData["expiration"]+" CVC: " +
                 getData["cvc"])
        tc10.confirm_payment_form().submit()
        self.explicit_wait_function_text('//strong[text()="4 (APPROVED)"]')
        self.redirect_to_start(self.env_path)

    @pytest.fixture(params=HomePageData.test_10_payment_no_mp)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param