#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_err106_25 import PaymentErr25
from TestData.Brana_test_data import HomePageData
import time
import pytest


class Test25(BaseClass):

    def test_standard_payment(self, getData):
        # Statická stránka pro err106 (zelená chyba)
        tc25 = PaymentErr25(self.driver)
        # logger
        log = self.getLogger()
        tc25.select_test().click()
        tc25.confirm_test().click()
        try:
            assert tc25.error_block().is_displayed()
            log.info("Message 106 displayed - Platba uhrazena - OK")
            state = True

        except:
            log.warning("No message - NOK")
            state = False
        assert state
        self.redirect_to_start(self.env_path)

    @pytest.fixture(params=HomePageData.test_14_bet_api)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param