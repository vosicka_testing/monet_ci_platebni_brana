#!/usr/bin/python
# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By


class PlatbaNaMiru11:

    def __init__(self, driver):
        self.driver = driver

    payment_form = (By.TAG_NAME, "form")
    link_test = (By.PARTIAL_LINK_TEXT, "Platba na míru")
    confirm_test_btn = (By.TAG_NAME, "input")
    payment_methods_btn = (By.CSS_SELECTOR, ".payment-methods a")
    card_number = (By.ID, "cardnumber")
    expiration = (By.ID, "expiry")
    card_number_popup_error = (By.CLASS_NAME, "card-brand-menu")
    cvc = (By.ID, "cvc")
    save_card_checkbox = (By.ID, "saveflag")

    def select_test(self):
        return self.driver.find_element(*PlatbaNaMiru11.link_test)

    def confirm_test(self):
        return self.driver.find_element(*PlatbaNaMiru11.confirm_test_btn)

    def payment_methods(self):
        payment_btns = self.driver.find_elements(*PlatbaNaMiru11.payment_methods_btn)
        return payment_btns

    def fill_card_number(self):
        return self.driver.find_element(*PlatbaNaMiru11.card_number)

    def click_show_popup_error(self):
        return self.driver.find_element(*PlatbaNaMiru11.card_number_popup_error)

    def fill_expiration(self):
        return self.driver.find_element(*PlatbaNaMiru11.expiration)

    def fill_cvc(self):
        return self.driver.find_element(*PlatbaNaMiru11.cvc)

    def save_card_check(self):
        return self.driver.find_element(*PlatbaNaMiru11.save_card_checkbox)

    def confirm_payment_form(self):
        return self.driver.find_element(*PlatbaNaMiru11.payment_form)
