#!/usr/bin/python
# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By


class PaymentVisa05:

    def __init__(self, driver):
        self.driver = driver

    payment_form = (By.TAG_NAME, "form")
    link_test = (By.PARTIAL_LINK_TEXT, "Platba pouze VISA kartou z EU regionu (err208 obecná)")
    confirm_test_btn = (By.TAG_NAME, "input")
    payment_methods_btn = (By.CSS_SELECTOR, ".payment-methods a")
    card_number = (By.ID, "cardnumber")
    card_number_popup_error = (By.CLASS_NAME, "card-brand-menu")
    card_number_popup = (By.CLASS_NAME, "error-popup-cardnumber")
    expiration = (By.ID, "expiry")
    cvc = (By.ID, "cvc")
    specific_card_brand = (By.CSS_SELECTOR, ".card-brand.VISA")

    def select_test(self):
        return self.driver.find_element(*PaymentVisa05.link_test)

    def confirm_test(self):
        return self.driver.find_element(*PaymentVisa05.confirm_test_btn)

    def payment_methods(self):
        payment_btns = self.driver.find_elements(*PaymentVisa05.payment_methods_btn)
        return payment_btns

    def fill_card_number(self):
        return self.driver.find_element(*PaymentVisa05.card_number)

    def click_show_popup_error(self):
        return self.driver.find_element(*PaymentVisa05.card_number_popup_error)

    def check_popup_error(self):
        return self.driver.find_element(*PaymentVisa05.card_number_popup).is_displayed()

    def fill_expiration(self):
        return self.driver.find_element(*PaymentVisa05.expiration)

    def fill_cvc(self):
        return self.driver.find_element(*PaymentVisa05.cvc)

    def check_visa_icon(self):
        return self.driver.find_element(*PaymentVisa05.specific_card_brand).is_displayed()

    def confirm_payment_form(self):
        return self.driver.find_element(*PaymentVisa05.payment_form)

