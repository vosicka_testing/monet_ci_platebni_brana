#!/usr/bin/python
# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By


class PaymentErr23:

    def __init__(self, driver):
        self.driver = driver

    link_test = (By.PARTIAL_LINK_TEXT, "Statická stránka pro err602")
    confirm_test_btn = (By.TAG_NAME, "p a")
    error_element = (By.XPATH, "//*[text()='We can not process transaction due to unexpected technical "
                               "difficulties']")

    def select_test(self):
        return self.driver.find_element(*PaymentErr23.link_test)

    def confirm_test(self):
        return self.driver.find_element(*PaymentErr23.confirm_test_btn)

    def error_block(self):
        return self.driver.find_element(*PaymentErr23.error_element)
