#!/usr/bin/python
# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By


class PaymentPtCsob21:

    def __init__(self, driver):
        self.driver = driver

    link_test = (By.PARTIAL_LINK_TEXT, "Platba pomocí pt@shop (Poštovní spořitelna)")
    confirm_test_btn = (By.TAG_NAME, "input")
    payment_methods_btn = (By.CSS_SELECTOR, ".payment-methods a")
    save_card_checkbox = (By.ID, "saveflag")
    payment_link = (By.CSS_SELECTOR, "a")

    def process_buttons(self):
        return self.driver.find_elements(*PaymentPtCsob21.payment_link)

    def select_test(self):
        return self.driver.find_element(*PaymentPtCsob21.link_test)

    def confirm_test(self):
        return self.driver.find_element(*PaymentPtCsob21.confirm_test_btn)

    def payment_methods(self):
        payment_btns = self.driver.find_elements(*PaymentPtCsob21.payment_methods_btn)
        return payment_btns

    def save_card_check(self):
        return self.driver.find_element(*PaymentPtCsob21.save_card_checkbox)

