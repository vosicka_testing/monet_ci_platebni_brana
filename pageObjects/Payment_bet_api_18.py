#!/usr/bin/python
# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By


class PaymentBetApi18:

    def __init__(self, driver):
        self.driver = driver

    payment_form = (By.CSS_SELECTOR, "button[type='submit']")
    link_test = (By.PARTIAL_LINK_TEXT, "Platba registrovanou kartou u sázkovky")
    confirm_test_btn = (By.TAG_NAME, "input")
    cvc = (By.ID, "saved-card-cvc")
    payment_methods_btn = (By.CSS_SELECTOR, ".payment-methods a")
    save_card_checkbox = (By.ID, "saveflag")

    def select_test(self):
        return self.driver.find_element(*PaymentBetApi18.link_test)

    def confirm_test(self):
        return self.driver.find_element(*PaymentBetApi18.confirm_test_btn)

    def fill_cvc(self):
        return self.driver.find_element(*PaymentBetApi18.cvc)

    def payment_methods(self):
        payment_btns = self.driver.find_elements(*PaymentBetApi18.payment_methods_btn)
        return payment_btns

    def save_card_check(self):
        return self.driver.find_element(*PaymentBetApi18.save_card_checkbox)

    def confirm_payment_form(self):
        return self.driver.find_element(*PaymentBetApi18.payment_form)

