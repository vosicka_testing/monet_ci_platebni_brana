#!/usr/bin/python
# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By


class PaymentErr27:

    def __init__(self, driver):
        self.driver = driver

    link_test = (By.PARTIAL_LINK_TEXT, "Statická stránka pro err108 (červená chyba)")
    confirm_test_btn = (By.TAG_NAME, "input")
    error_element = (By.XPATH, "//*[text()='Platba zrušena']")

    def select_test(self):
        return self.driver.find_element(*PaymentErr27.link_test)

    def confirm_test(self):
        return self.driver.find_element(*PaymentErr27.confirm_test_btn)

    def error_block(self):
        return self.driver.find_element(*PaymentErr27.error_element)
